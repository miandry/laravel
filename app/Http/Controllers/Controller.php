<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use Illuminate\Filesystem;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function save(Request $request)
      {
          $results = [];
          if(isset($request->price)&&isset($request->name)&&isset($request->qte)){
              $name = $request->name;
              $qte = $request->qte;
              $price = $request->price;
              $path = storage_path('data/data.json');

              $content = json_decode(file_get_contents($path), true);

              $array = [
                  "id"=> sizeof($content),
                  "name"=>$name,
                  "qte"=>$qte,
                  "price"=>$price
              ];
              $content[] = $array;
              $json_result = json_encode($content);
              file_put_contents($path,$json_result);
              $results = json_decode(file_get_contents($path), true);
          }

          return  response()->json($results);
      }
        public function listItems(Request $request)
        {
            $path = storage_path('data/data.json');
            $results = json_decode(file_get_contents($path), true);
            return  response()->json($results);
        }
}
