<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="/public/library/bootstrap/css/bootstrap.min.css"/>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="panel panel-primary col-xs-12">
            <div class="panel-header"><h2 class="col-xs-12"> Insert Item Product</h2></div>
            <div class="panel-body">
                <form id="insert-product" action="{{ Request::root()  }}/api/save/product">
                    <div class="form-group">
                        <label for="produit-name">Name</label>
                        <input type="text" class="form-control" id="produit-name" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label for="produit-qte">Quantity</label>
                        <input type="text" class="form-control" id="produit-qte" placeholder="Quantity">
                    </div>
                    <div class="form-group">
                        <label for="produit-price">Price</label>
                        <input type="text" class="form-control" id="produit-price" placeholder="Price">
                    </div>
                    <button type="button" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <table id="table-product" data-url="{{ Request::root()  }}/api/list/product" class="table table-condensed">

            <tr>
                <th>Name</th>
                <th>Quantity</th>
                <th>Price</th>
            </tr>

            <tbody class="main">
              <tr>
                  <td>1</td>
                  <td>2</td>
                  <td>2</td>
              </tr>
            </tboby>
        </table>
    </div>

        </div>
        <script src="/public/library/jquery/jquery.min.js"></script>
        <script src="/public/library/bootstrap/js/bootstrap.min.js"></script>
        <script src="/public/js/custom.js"></script>
</body>

</html>
