var save_product = function (url){
   // alert(url);
    $.ajax({
        'url': url ,
        'success': function (data) {
            json = data;
            console.log(data);
        }
    });
}
var load_products = function (){
    var url = jQuery("#table-product").attr("data-url");
    $.ajax({
        'url': url ,
        'success': function (data) {
            json = data;
            var str = "";
            $.each(json, function(i, item) {
                str=str+"<tr>";
                str=str+"<td>";
                str=str+ item.name;
                str=str+"</td>";
                str=str+"<td>";
                str=str+ item.qte;
                str=str+"</td>";
                str=str+"<td>";
                str=str+ item.price;
                str=str+"</td>";
                str=str+"</tr>";
            });
           // jQuery("#table-product").find("tbody").ht
            jQuery("#table-product").find(".main").html(str);

        }
    });
}
var generate_url = function(root_url){
    var name = jQuery("#produit-name").val();
    var qte = jQuery("#produit-qte").val();
    var price = jQuery("#produit-price").val();
    return root_url+"?name="+name+"&qte="+qte+"&price="+price ;
}

jQuery("#insert-product").find("button").click(function(){
    var root_url = jQuery("#insert-product").attr("action");
    var url = generate_url(root_url);
    jQuery(this).text("Saving ...");
    save_product(url);
    jQuery(this).text("Save");
});
jQuery(document).ajaxStop(function () {
    load_products();
});
jQuery(document).ready(function () {
    load_products();
});